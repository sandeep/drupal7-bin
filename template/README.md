# example.com

clone with `git clone --recursive git@gitlab.ebi.ac.uk:ebiwd/drupal-sites/tsc.ebi.ac.uk.git`

## Development

See https://gitlab.ebi.ac.uk/ebiwd/drupal-bin/blob/master/README.md for information about development

```bin/dev``` make will collect external modules specified in makefiles, and mix these with custom code in dist/sites/[sitename]/modules/custom and dist/sites/[sitename]/themes/custom.
## Deployment

- All commits are automatically deployed to dev and stage
- All tags are automatically deployed to prod
