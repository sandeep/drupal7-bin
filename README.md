# Drupal Development Tools

This folder contains scripts used to aid local drupal development, it should be installed as a submodule in the bin directory of a drupal project.

**Please note all current Drupal7 sites are being updated/installed using composer file, so we are no more using make files.**

## Installation

`git submodule add --force https://gitlab.ebi.ac.uk/ebiwd/drupal7-bin.git bin`

## Update

`git submodule update --remote --force bin`

## Usage

- `bin/dev <options>` - spin up/down local development environment
- `bin/dump <options>` - pull database and/or files from VMs
- `bin/deploy <options>` - deploy source code to VMs

See each script for help with options.

## Template files

Copy the template files to the root of the drupal repo, eg `cp -rp template/. .`, and edit .env and README.md to customise the repo.

## Typical workflow for updating/testing an existing drupal site

- `git clone --recursive git@gitlab.ebi.ac.uk:ebiwd/<repo>` - clone drupal site to local machine
- `bin/dev quick` is an macro for
  - `bin/dev create-aliases`
  - `bin/dump sql prod`
  - `bin/dump files prod`
  - `bin/dev up`
  - `bin/dev login`
- edit composer.json to update or add any modules
- `bin/dev down` - to spin down docker containers
- review and commit composer.json & .lock file and respective updated modules to git repo


## Typical workflow for creating a new site

- `rm -rf _*` - to clear existing database and files
- `bin/dev up` - to spin up docker containers for local development
- `bin/dev install` - to run site installer
- `bin/dev launch` - to launch browser
- `bin/dev down` - to spin down docker containers

## Diagnostics

- `bin/dev logs` - tail logs from containers
- `bin/dev mailhog` - launch mailhog to view mail sent my containers
- `bin/dev pma` - launch phpMyAdmin to view database

## Pre-requisites (mac)

You will need
- [Docker Community Edition](https://www.docker.com/community-edition#/download) insalled on your development machine
- [drush 8.1](http://docs.drush.org/en/8.x/install-alternative), which will require
  - [composer](https://getcomposer.org/doc/00-intro.md) installed globally
  - php7 (installed with [homebrew](https://brew.sh/) on osx)
- SSH keys for drupal apache users in ~/.ssh, and ~/.ssh/config configured with
```
Host ves-ebi-?? ves-hx-?? ves-pg-?? ves-oy-??
  IdentityFile ~/.ssh/%r
  StrictHostKeyChecking no
  BatchMode yes
```

# Pre-requisites (windows)

You will need
- [Docker Community Edition](https://www.docker.com/community-edition#/download) insalled on your development machine
- [Git Bash](https://git-for-windows.github.io/)
- [drush 8.1](http://docs.drush.org/en/8.x/install-alternative), which will require
    - [Composer](https://getcomposer.org/doc/00-intro.md#installation-windows)
    - [PHP7 binary](http://windows.php.net/downloads/releases/php-7.1.10-Win32-VC14-x64.zip)
